# Ecommerce Customers Segmentation

## Requirements

- Python >=3.0
- [Anaconda](https://www.anaconda.com/)

First create a custom-named environment:

    $ conda env create -f environment.yml -n ENV_NAME

Second, update the environment to ensure all the necessary packages for the project are installed :

    $ conda env update -f environment.yml -n ENV_NAME

Third, activate the environment :

    $ source activate ENV_NAME

Finally, open jupyter to view the project notebooks :

    $ jupyter notebook 


## Goal

In this project, an online retailer based in the UK gave us a dataset past purchase data at the transaction level. In the scope of their customer segmentation initiave, they ask us to build a clustering model. This model should factor in both aggregate sales patterns and specific items purchased.

## Approach

After exploring and cleaning the given dataset with key features, we applied K-Means clustering method. Because of the high number of features, we used some popularity tresholds (to extract the top 20 products purchased and grouped by customers) and PCA respectively to reduce those number of features. 


## Results

Based on a higher similarity score, we noticed that clustering applied on data with features-reduced (with PCA) was more similar to the one applied on our analytical base table (clean dataset). 

![model](assets/base-cluster.png)

## Author

MAKONG Ludovic / [@ldmakong](https://gitlab.com/ldmakong)


